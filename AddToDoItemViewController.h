//
//  AddToDoItemViewController.h
//  ToDoList
//
//  Created by li on 4/10/14.
//
//

#import <UIKit/UIKit.h>
#import "ToDoItem.h"

@interface AddToDoItemViewController : UIViewController
@property ToDoItem *toDoItem;
@end
