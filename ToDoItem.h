//
//  ToDoItem.h
//  ToDoList
//
//  Created by li on 4/13/14.
//
//

#import <Foundation/Foundation.h>

@interface ToDoItem : NSObject

@property NSString *itemName;
@property BOOL completed;
@property (readonly) NSDate *creationDate;

@end
