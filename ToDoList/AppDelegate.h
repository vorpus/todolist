//
//  AppDelegate.h
//  ToDoList
//
//  Created by li on 4/1/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
